pw-dump
#######

-------------------------
The PipeWire state dumper
-------------------------

:Manual section: 1
:Manual group: General Commands Manual

SYNOPSIS
========

| **pw-dump** [*options*]

DESCRIPTION
===========

The *pw-dump* program produces a representation of the current
PipeWire state as JSON, including the information on nodes, devices,
modules, ports, and other objects.

OPTIONS
=======

-h | --help
  Show help.

-r | --remote=NAME
  The name of the *remote* instance to dump. If left unspecified,
  a connection is made to the default PipeWire instance.

-m | --monitor
  Monitor PipeWire state changes, and output JSON arrays describing changes.

-N | --no-colors
  Disable color output.

-C | --color=WHEN
  Whether to enable color support. WHEN is ``never``, ``always``, or ``auto``.


AUTHORS
=======

The PipeWire Developers <@PACKAGE_BUGREPORT@>; PipeWire is available from @PACKAGE_URL@

SEE ALSO
========

``pipewire(1)``,
``pw-cli(1)``,
``pw-top(1)``,

